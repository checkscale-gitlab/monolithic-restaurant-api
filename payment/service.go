package payment

type IService interface {
	GetPayments(query GetPaymentsQuery) ([]Payment, error)
	AddPayment(request CreatePaymentRequest) (Payment, error)
	GetPaymentByID(paymentID int) (Payment ,error)
	DeletePayment(paymentID int) error
	UpdatePayment(paymentID int, request UpdatePaymentRequest) (Payment, error)
}
type Service struct {
	repo IRepository
}

func NewService(repository IRepository) *Service {
	return &Service{repo: repository}
}

func (s Service) GetPayments(query GetPaymentsQuery) ([]Payment, error) {
	return s.repo.GetPayments(query)
}

func (s Service) AddPayment(request CreatePaymentRequest) (Payment, error) {
	return s.repo.AddPayment(request)
}

func (s Service) GetPaymentByID(paymentID int) (Payment, error) {
	return s.repo.GetPaymentByID(paymentID)
}

func (s Service) DeletePayment(paymentID int) error {
	return s.repo.DeletePayment(paymentID)
}

func (s Service) UpdatePayment(paymentID int, request UpdatePaymentRequest) (Payment, error) {
	return s.repo.UpdatePayment(paymentID,request)
}
