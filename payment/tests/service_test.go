package tests

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"restaurant-api/payment"
	"restaurant-api/payment/mocks"
	"testing"
)

// we need to mock repository that service uses before test it.
//we need to test then implement:
/*
	NewService Method
	GetPayments(query GetPaymentsQuery) (Payment, error)
	AddPayment(request CreatePaymentRequest) (Payment, error)
	GetPaymentByID(paymentID int) (Payment ,error)
	DeletePayment(paymentID int) error
	UpdatePayment(paymentID int, request UpdatePaymentRequest) (Payment, error)
*/
/*
	The nice way to this in step by step and TDD approach is creating an IService interface,
	then every function that we write test for, will add there and then implemented in service.
*/
func TestNewServiceCreatedByGivenRepositorySuccessfully(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)
}

func TestIGetPaymentsFromRepositoryByGivenQuery(t *testing.T) {
	test := struct {
		query payment.GetPaymentsQuery
		expectedPayments []payment.Payment
	}{
		query: payment.GetPaymentsQuery{PaymentType: "testType"},
		expectedPayments: []payment.Payment{
			{
				ID:            2,
				CustomerID:    3,
				OrderID:       4,
				PaymentTypeID: 2,
				PaymentType:   payment.PaymentType{ID: 2, Name: "testType"},
			},
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepo := mocks.NewMockIRepository(mockCtrl)
	mockRepo.EXPECT().GetPayments(test.query).Return(test.expectedPayments, nil)

	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)

	payments,err := service.GetPayments(test.query)
	assert.Nil(t, err)
	assert.Equal(t, len(test.expectedPayments), len(payments))
}

func TestIGetCreatedPaymentWhenAddPaymentCalledWithValidCreatePaymentRequest(t *testing.T) {
	test := struct {
		request payment.CreatePaymentRequest
		expectedPayment payment.Payment
	}{
		request: payment.CreatePaymentRequest{
			OrderID:       2,
			CustomerID:    4,
			PaymentTypeID: 5,
		},
		expectedPayment: payment.Payment{
				ID:            22,
				CustomerID:    4,
				OrderID:       2,
				PaymentTypeID: 5,
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepo := mocks.NewMockIRepository(mockCtrl)
	mockRepo.EXPECT().AddPayment(test.request).Return(test.expectedPayment, nil)

	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)

	addedPayment, err := service.AddPayment(test.request)
	assert.Nil(t, err)
	assert.Equal(t, test.expectedPayment, addedPayment)
}

func TestIGetPaymentWhenGetPaymentByIDCalledWithValidPaymentID(t *testing.T) {
	test := struct {
		paymentID int
		expectedPayment payment.Payment
	}{
		paymentID: 22,
		expectedPayment: payment.Payment{
			ID:            22,
			CustomerID:    4,
			OrderID:       2,
			PaymentTypeID: 5,
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepo := mocks.NewMockIRepository(mockCtrl)
	mockRepo.EXPECT().GetPaymentByID(test.paymentID).Return(test.expectedPayment, nil)

	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)

	gotPayment,err := service.GetPaymentByID(test.paymentID)
	assert.Nil(t, err)
	assert.Equal(t, test.expectedPayment,gotPayment)
}

func TestPaymentDeleteSuccessfullyWhenDeletePaymentCalledWithExistPaymentIdAndNotReturnsError(t *testing.T) {
	test := struct {
		paymentID int
	}{
		paymentID: 22,
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepo := mocks.NewMockIRepository(mockCtrl)
	mockRepo.EXPECT().DeletePayment(test.paymentID).Return(nil)

	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)

	err := service.DeletePayment(test.paymentID)
	assert.Nil(t, err)
}

func TestPaymentUpdatedSuccessfullyWhenUpdatePaymentCalledWithValidRequestAndReturnsUpdatedPayment(t *testing.T) {
	test := struct {
		paymentID int
		request payment.UpdatePaymentRequest
		expectedPayment payment.Payment
	}{
		paymentID: 22,
		request: payment.UpdatePaymentRequest{
			OrderID:       453,
			CustomerID:    123,
			PaymentTypeID: 54,
		},
		expectedPayment: payment.Payment{
			ID:            22,
			CustomerID:    123,
			OrderID:       453,
			PaymentTypeID: 54,
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRepo := mocks.NewMockIRepository(mockCtrl)
	mockRepo.EXPECT().UpdatePayment(test.paymentID,test.request).Return(test.expectedPayment, nil)

	service := payment.NewService(mockRepo)
	assert.NotNil(t, service)

	updatedPayment, err := service.UpdatePayment(test.paymentID, test.request)
	assert.Nil(t, err)
	assert.Equal(t, test.expectedPayment,updatedPayment)
}