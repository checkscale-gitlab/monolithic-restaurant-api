package config


type DB struct{
	Driver string
	Username string
	Password string
	Host string
	Port string
	DBName string
	TablePrefix string
}

