package main

import (
	"fmt"
	"go.uber.org/zap"
	"log"
	"os"
	"restaurant-api/config"
	"restaurant-api/foods"
	"restaurant-api/order"
	"restaurant-api/payment"
	"restaurant-api/postgre"
	"restaurant-api/server"
)

func main(){
	//dbconfig := config.DB{
	//	Driver:   "postgres",
	//	Username: "postgres",
	//	Password: "mypassword",
	//	Host:     "0.0.0.0",
	//	Port:     "5432",
	//	DBName:   "postgres",
	//	TablePrefix: "order.",
	//}
	
	dbconfig := config.DB{
		Driver:   os.Getenv("DB_DRIVER"),
		Username: os.Getenv("DB_USERNAME"),
		Password: os.Getenv("DB_PASSWORD"),
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		DBName:   os.Getenv("DB_NAME"),
		TablePrefix: os.Getenv("DB_TABLE_PREFIX"),
	}
	fmt.Printf("Config file %v", dbconfig )
	db, err := postgre.New(dbconfig)
	//db, err := postgre.NewPostgreDB(dbconfig)
	if err != nil {
		log.Printf("%s", err.Error())
	}
	orderRepository := order.NewRepository(db)
	orderService := order.NewService(orderRepository, zap.NewExample())
	orderHandlers := order.NewHandler(orderService)

	foodsRepository := foods.NewRepository(db)
	foodsService := foods.NewService(foodsRepository)
	foodsHandlers := foods.NewHandler(foodsService,zap.NewExample())
	paymentRepository := payment.NewRepository(db)
	paymentService := payment.NewService(paymentRepository)
	paymentHandlers := payment.NewHandlers(paymentService)

	handlers := []server.Handler{
		orderHandlers,
		foodsHandlers,
		paymentHandlers,
	}
	server := server.NewServer(config.Server{ Port:os.Getenv("SERVER_PORT") },handlers)
	server.Run()
}
