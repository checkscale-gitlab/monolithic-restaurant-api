package postgre

import (
	"fmt"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
	"log"
	"restaurant-api/config"
)

func New(config config.DB) (*gorm.DB, error) {
	dsn := createDSN(config)
	db,err := gorm.Open(postgres.Open(dsn),&gorm.Config{
		SkipDefaultTransaction:                   false,
		NamingStrategy:                           schema.NamingStrategy{
			TablePrefix:   config.TablePrefix,
			SingularTable: true,
			NoLowerCase:   true,
		},
	})
	//when you pass not existing id or sth., it not updates the db to protect your data
	if err != nil {
		log.Fatalf("Error on opening db connection: %s", err.Error())
		return nil, err
	}
	return db, nil
}
//createDSN creates the source name, DSN refers source name in gorm.
func createDSN(config config.DB) string {
	return fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		config.Host, config.Port, config.Username, config.Password, config.DBName)
}

var DefaultConfig = config.DB{
	Driver:   "postgres",
	Username: "postgres",
	Password: "mypassword",
	Host:     "0.0.0.0",
	Port:     "5432",
	DBName:   "postgres",
	TablePrefix: "restaurant.",
}