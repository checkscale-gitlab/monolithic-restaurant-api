package order

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/stretchr/testify/assert"
	"log"
	"restaurant-api/config"
	"restaurant-api/postgre"
	"testing"
)

func TestCreateNewRepositoryWithGivenDB(t *testing.T) {
	type Test struct {
		Name string
		Expected interface{}
		Actual interface{}
		NotExpected interface{}
	}
	repository := NewRepository(nil)

	test := Test{
		Name: "return empty repository when given db is nil",
		Expected: &Repository{},
		Actual:     repository,
	}
	assert.Equal(t, test.Expected, test.Actual, test.Name)
}
func TestCreateNewOrderCorrectlyWithGivenParams(t *testing.T) {
	db, _ := postgre.New(config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	})
	repo := NewRepository(db)
	context,_ := context.WithTimeout(context.Background(),20)
	order ,err := repo.NewOrder(context,sql.TxOptions{},2,3,2)
	if err != nil {
		assert.Error(t, err, "Error occurred when getting orders from repo")
	}
	fmt.Printf("Order : %d", order.ID)
	assert.Equal(t, 2,2,"")
}
func TestGetOrdersCorrectlyGetOrdersFromDB(t *testing.T) {
	db, _ := postgre.New(config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	})
	repo := NewRepository(db)
	context,_ := context.WithTimeout(context.Background(),20)
	orders,err := repo.GetOrders(context, sql.TxOptions{})
	if err != nil {
		assert.Error(t, err, "Error occurred when getting orders from repo")
	}
	for _,order := range orders {
		fmt.Printf("OrderID: %d, FoodID: %d, CustomerID: %d, Cooker Name: %s \n", order.ID, order.FoodID, order.CustomerID, order.Cooker.Name)
	}
	assert.GreaterOrEqualf(t, len(orders),1 ,"")
}
func TestGetOrderByID(t *testing.T) {
	db, _ := postgre.New(config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	})
	repo := NewRepository(db)
	context,_ := context.WithTimeout(context.Background(),20)

	orderID := 2
	order,err := repo.GetOrderByID(context, sql.TxOptions{},orderID)
	if err != nil {
		assert.Error(t, err, "Error occurred when getting order by id from repo")
	}
	fmt.Printf("OrderID: %d, FoodID: %d, CustomerID: %d, CookerID: %d, CookerName: %s\n", order.ID, order.FoodID, order.CustomerID, order.CookerID, order.Cooker.Name)

	assert.Greater(t, order.FoodID, -1,"test food id is same that db has")
	assert.Greater(t, order.CustomerID, -1, "test customer id same that db has")
}
func TestUpdateOrderWithGivenFoodID(t *testing.T) {
	db, _ := postgre.New(config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	})
	repo := NewRepository(db)
	context,_ := context.WithTimeout(context.Background(),20)

	foodID := 10
	orderID := 7
	order,err := repo.UpdateOrderFoodID(context, sql.TxOptions{}, orderID ,foodID)
	if err != nil {
		assert.Error(t, err, "Error occurred when updating order food id from repo")
	}

	fmt.Printf("OrderID: %d, FoodID: %d, CustomerID: %d \n", order.ID, order.FoodID, order.CustomerID)

	assert.Equal(t, foodID,order.FoodID,"food id updated successfully in db")
}
func TestDeleteOrderByID(t *testing.T) {
	db, _ := postgre.New(config.DB{
		Driver:   "postgres",
		Username: "postgres",
		Password: "mypassword",
		Host:     "localhost",
		Port:     "5432",
		DBName:   "postgres",
	})
	repo := NewRepository(db)
	context,_ := context.WithTimeout(context.Background(),20)

	orderID := 2
	repo.DeleteOrder(context, sql.TxOptions{}, orderID)
	order,err := repo.GetOrderByID(context,sql.TxOptions{}, orderID)
	if err != nil {
		log.Println("Error occurred when get order from repo by id")
	}

	assert.Equal(t, order.ID,0,"food id deleted successfully in db")
}