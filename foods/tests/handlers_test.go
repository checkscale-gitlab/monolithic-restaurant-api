// +build test

package tests

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"
	"io"
	"net/http"
	"net/http/httptest"
	"restaurant-api/foods"
	"restaurant-api/foods/mocks"
	"strconv"
	"testing"
)
const FoodsRoute = "/foods"
/* TestWhenGetFoodsCalledItReturnAllFoods tests that get foods
handler successfully registered to app and gives the expected result
with given mock service.
*/
func TestWhenGetFoodsCalledItReturnAllFoods(t *testing.T) {
	expectedFoods := []foods.Food{
		{
			ID:         2,
			Name:       "Fruit",
			Price:      20,
			CategoryID: 2,
			Category:   foods.Category{},
		},
		{
			ID:         3,
			Name:       "Vegetable",
			Price:      30,
			CategoryID: 3,
			Category:   foods.Category{},
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().GetFoods().Return(expectedFoods ,nil)
	handlers := foods.NewHandler(mockService,zap.NewExample())
	app := fiber.New()
	handlers.RegisterRoutes(app)
	getFoodsRequest := MakeRequestWithoutBody(fiber.MethodGet, FoodsRoute)
	resp, err := app.Test(getFoodsRequest)

	assert.Nil(t, err, "Error occurred app.Test request")
	assertBodyEqual(t,resp.Body, expectedFoods)
}
func TestWhenGetFoodsByIDCalledWithExistFoodIdItReturnsFoodInSuccess(t *testing.T) {
	test := struct {
		foodID int
		expectedFood foods.Food
	}{
		foodID: 5,
		expectedFood: foods.Food{
			ID:         5,
			Name:       "test food",
			Price:      2,
			CategoryID: 2,
			Category:   foods.Category{},
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().GetFoodByID(test.foodID).Return(test.expectedFood, nil)
	handlers := foods.NewHandler(mockService,zap.NewExample())
	app := fiber.New()
	handlers.RegisterRoutes(app)

	req := MakeRequestWithoutBody(fiber.MethodGet, makeUrlWithParam(FoodsRoute, strconv.Itoa(test.foodID)))
	resp, err := app.Test(req)
	assert.Nil(t, err, "Error occurred when sending request")
	assertBodyEqual(t, resp.Body, test.expectedFood)
}
func TestWhenUpdateRequestCameWithTrueParametersItReturnsUpdatedFoodInSuccess(t *testing.T) {
	test := struct {
		foodID int
		updateRequest foods.UpdateFoodRequest
		expectedFood foods.Food
	}{
		foodID:2,
		updateRequest: foods.UpdateFoodRequest{
			FoodID:     2,
			FoodName:   "test updated food",
			FoodPrice:  3,
			CategoryID: 2,
		},
		expectedFood: foods.Food{
			ID:         2,
			Name:       "test updated food",
			Price:      3,
			CategoryID: 2,
			Category:   foods.Category{},
		},
	}

	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().UpdateFood(test.foodID,test.updateRequest).Return(test.expectedFood, nil)
	handlers := foods.NewHandler(mockService,zap.NewExample())
	app := fiber.New()
	handlers.RegisterRoutes(app)

	req := MakeRequestWithBody(fiber.MethodPut,makeUrlWithParam(FoodsRoute,strconv.Itoa(test.foodID)),test.updateRequest)
	resp,err := app.Test(req)

	assert.Nil(t, err,"Error occurred on testing send the request")
	assertBodyEqual(t,resp.Body,test.expectedFood)
}
func TestWhenDeleteRequestCameWithFoodIDItDeletesFoodAndReturnsSuccessCode(t *testing.T) {
	test := struct {
		foodID int
	}{
		foodID: 4,
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().DeleteFood(test.foodID).Return(nil)
	handlers := foods.NewHandler(mockService, zap.NewExample())
	app := fiber.New()
	handlers.RegisterRoutes(app)

	req := MakeRequestWithoutBody(fiber.MethodDelete, makeUrlWithParam(FoodsRoute, strconv.Itoa(test.foodID)))
	resp, err := app.Test(req)

	assert.Nil(t, err)
	assert.Equal(t, resp.StatusCode, fiber.StatusOK)
}
func TestWhenCreateFoodRequestCameItShouldCreateAndReturnNewFoodSuccessfully(t *testing.T) {
	test := struct {
		request foods.CreateFoodRequest
		expectedFood foods.Food
	}{
		request: foods.CreateFoodRequest{
			FoodName:   "test food",
			FoodPrice:  20,
			CategoryID: 30,
		},
		expectedFood: foods.Food{
			ID:         20,
			Name:       "test food",
			Price:      20,
			CategoryID: 30,
			Category:   foods.Category{},
		},
	}
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockService := mocks.NewMockIService(mockCtrl)
	mockService.EXPECT().AddFood(test.request).Return(test.expectedFood, nil)

	handlers := foods.NewHandler(mockService, zap.NewExample())
	app := fiber.New()
	handlers.RegisterRoutes(app)

	req := MakeRequestWithBody(fiber.MethodPost,FoodsRoute,test.request)
	resp, err := app.Test(req)

	assert.Nil(t, err)
	assertBodyEqual(t,resp.Body,test.expectedFood)
}
func MakeRequestWithoutBody(method, url string) *http.Request{
	req := httptest.NewRequest(method, url, http.NoBody)
	req.Header.Add("Content-type", "application/json")
	return req
}

func MakeRequestWithBody(method, url string, body interface{}) *http.Request{
	bodyAsByte, _ := json.Marshal(body)
	req := httptest.NewRequest(method, url, bytes.NewReader(bodyAsByte))
	req.Header.Add("Content-type", "application/json")
	return req
}
func makeUrlWithParam(baseUrl, param string) string{
	return fmt.Sprintf("%s/%s", baseUrl, param)
}
func assertBodyEqual(t *testing.T, responseBody io.Reader,expectedValue interface{}) {
	var actualBody interface{}
	_ = json.NewDecoder(responseBody).Decode(&actualBody)
	expectedBodyAsJSON, _ := json.Marshal(expectedValue)
	var expectedBody interface{}
	_ = json.Unmarshal(expectedBodyAsJSON, &expectedBody)
	assert.Equal(t, expectedBody, actualBody)

}