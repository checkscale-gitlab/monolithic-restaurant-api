// +build test


package tests

import (
	"context"
	"database/sql"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"log"
	"restaurant-api/foods"
	"restaurant-api/foods/mocks"
	"testing"
)

func TestServiceCreatedSuccessfullyWithGivenRepository(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	assert.NotEqual(t, nil , service , "Service created with new function successfully")
}
func TestServiceGetsAllFoodsSuccessfully(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	mockRepo.EXPECT().GetFoods(context.Background(),sql.TxOptions{}).Return([]foods.Food{{ID: 1, Name: "test food", Price: 24, CategoryID: 2, Category:foods.Category{} }}, nil)
	foods,err := service.GetFoods()
	if err != nil {
		log.Printf("\n Error occured: %s",err.Error())
	}
	log.Printf("Test food :%v", foods[0])
	assert.Equal(t, len(foods), 1)
}
func TestServiceGetsFoodWithGivenFoodID(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	mockRepo.EXPECT().GetFoodByID(context.Background(),sql.TxOptions{},2).Return(foods.Food{ID: 2, Name: "test food", Price: 24, CategoryID: 2, Category:foods.Category{} }, nil)
	food,err := service.GetFoodByID(2)
	if err != nil {
		log.Printf("\n Error occured: %s",err.Error())
	}
	log.Printf("Test food :%v", food)
	assert.Equal(t, food.Name, "test food")
}
func TestServiceUpdatesAndReturnsFoodByGivenRequestSuccessfully(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	type Test struct{
		foodID int
		request foods.UpdateFoodRequest
		updatedFood foods.Food
	}
	test := Test{
		foodID:  2,
		request: foods.UpdateFoodRequest{
			FoodID:     2,
			FoodName:   "test food",
			FoodPrice:  20,
			CategoryID: 2,
		},
		updatedFood: foods.Food{
			ID:         2,
			Name:       "test food",
			Price:      20,
			CategoryID: 2,
			Category:   foods.Category{},
		},
	}
	mockRepo.EXPECT().UpdateFood(context.Background(),sql.TxOptions{},test.foodID,test.request).Return(test.updatedFood, nil)
	food,err := service.UpdateFood(test.foodID,test.request)
	if err != nil {
		log.Printf("\n Error occured: %s",err.Error())
	}
	log.Printf("Test food :%v", food)
	assert.Equal(t, food.Name, "test food")
}
func TestServiceAddGivenFoodToDbSuccessfully(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	type Test struct{
		request foods.CreateFoodRequest
		addedFood foods.Food
	}
	test := Test{
		request: foods.CreateFoodRequest{
			FoodName:   "testing food",
			FoodPrice:  20,
			CategoryID: 2,
		},
		addedFood: foods.Food{
			ID:         12,
			Name:       "testing food",
			Price:      20,
			CategoryID: 2,
			Category:   foods.Category{},
		},
	}
	mockRepo.EXPECT().AddFood(context.Background(),sql.TxOptions{},test.request).Return(test.addedFood, nil)
	food,err := service.AddFood(test.request)
	if err != nil {
		log.Printf("\n Error occured: %s",err.Error())
	}
	log.Printf("Test food :%v", food)
	assert.Equal(t, food.Name, test.request.FoodName)
}
func TestServiceDeletesFoodFromDBSuccessfullyByFoodID(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()
	mockRepo := mocks.NewMockIRepository(mockCtrl)
	service := foods.NewService(mockRepo)
	type Test struct{
		foodID int
	}
	test := Test{
		foodID: 2,
	}
	mockRepo.EXPECT().DeleteFood(context.Background(),sql.TxOptions{},test.foodID).Return(nil)
	err := service.DeleteFood(test.foodID)
	if err != nil {
		log.Printf("\n Error occured: %s",err.Error())
	}
	assert.Equal(t, err, nil,"Deleting food from db without error")
}