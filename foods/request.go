package foods

type UpdateFoodRequest struct {
	FoodID int `json:"foodID"`
	FoodName string `json:"foodName"`
	FoodPrice int `json:"foodPrice"`
	CategoryID int `json:"categoryID"`
}
type CreateFoodRequest struct {
	FoodName string `json:"foodName"`
	FoodPrice int `json:"foodPrice"`
	CategoryID int `json:"categoryID"`
}
