package foods

import (
	"context"
	"database/sql"
	"gorm.io/gorm"
	"log"
)

type IRepository interface {
	GetFoods(ctx context.Context, options sql.TxOptions) ([]Food, error)
	AddFood(ctx context.Context, options sql.TxOptions, request CreateFoodRequest) (Food, error)
	UpdateFood(ctx context.Context, options sql.TxOptions, foodID int, request UpdateFoodRequest) (Food, error)
	DeleteFood(ctx context.Context, options sql.TxOptions, foodID int) error
	GetFoodByID(ctx context.Context, options sql.TxOptions, foodID int) (Food, error)
}
type Repository struct {
	db * gorm.DB
}
func NewRepository(db *gorm.DB) *Repository{
	return &Repository{db:db}
}
func (r *Repository) GetFoods(ctx context.Context, options sql.TxOptions) ([]Food, error) {
	var foods []Food
	r.db.Transaction(
		func(tx *gorm.DB) error {
			if err := tx.Model(&Food{}).Find(&foods).Error; err != nil {
				return err
			}
			return nil // return nil commits the transaction.Otherwise, any error causes rollback
		})
	return foods, nil
}
func (r *Repository) AddFood(ctx context.Context, options sql.TxOptions,request CreateFoodRequest) (Food, error){
	food := Food{CategoryID: request.CategoryID, Name: request.FoodName, Price: request.FoodPrice}
	r.db.Transaction(
		func(tx *gorm.DB) error {
			if err := tx.Create(&food).Error; err != nil {
				return err
			}
			return nil
		})
	return food, nil
}
func (r *Repository) UpdateFood(ctx context.Context, options sql.TxOptions,foodID int, request UpdateFoodRequest) (Food, error) {
	food := Food{ID: foodID, Name: request.FoodName, CategoryID: request.CategoryID}
	r.db.Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&food).Error
		if err != nil {
			log.Printf("Error occured on updating food: %s",err.Error())
			return err
		}
		return nil
	})
	updatedFood, err := r.GetFoodByID(ctx, options, request.FoodID)
	if err != nil {
		return Food{}, err
	}
	return updatedFood, nil
}
func (r *Repository) GetFoodByID(ctx context.Context, options sql.TxOptions, foodID int) (Food, error){
	var food Food
	err := r.db.Transaction(func(tx *gorm.DB) error {
		err := tx.Preload(CATEGORY).Where(&Food{ID: foodID}, "ID").Find(&food).Error
		if err != nil {
			log.Printf("Error occured when getting food by id: %s", err.Error())
			return err
		}
		return nil
	})
	if err != nil {
		return Food{}, err
	}
	return food, nil
}
func (r *Repository) DeleteFood(ctx context.Context, options sql.TxOptions, foodID int) error {
	err := r.db.Transaction(func(tx *gorm.DB) error {
		err := tx.Delete(&Food{}, foodID).Error
		if err != nil {
			return err
		}
		return nil
	})
	if err != nil {
		return err
	}
	return nil
}