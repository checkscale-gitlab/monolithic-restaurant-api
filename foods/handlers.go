package foods

import (
	"github.com/gofiber/fiber/v2"
	"go.uber.org/zap"
)

type Handler struct {
	service IService
	logger *zap.Logger
}
func NewHandler(service IService, logger *zap.Logger) *Handler {
	return &Handler{service: service}
}
//GetFoods gets the foods
func (h *Handler) GetFoods(c *fiber.Ctx) error {
	foods,err := h.service.GetFoods()
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(foods)
}
func (h *Handler) AddFood(c *fiber.Ctx) error {
	var request CreateFoodRequest
	if err := c.BodyParser(&request); err!= nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	food,err := h.service.AddFood(request)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusCreated).JSON(food)
}
func (h *Handler) GetFoodByID(c *fiber.Ctx) error {
	foodID, err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	var food Food
	food,err = h.service.GetFoodByID(foodID)
	if err != nil {
		return c.SendStatus(fiber.StatusNotFound)
	}
	return c.Status(fiber.StatusOK).JSON(food)
}
func (h *Handler) DeleteFood(c *fiber.Ctx) error {
	foodID, err := c.ParamsInt("id")
	if err != nil {
		return c.SendStatus(fiber.StatusBadRequest)
	}
	err = h.service.DeleteFood(foodID)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.SendStatus(fiber.StatusOK)
}
func (h *Handler) UpdateFood(c *fiber.Ctx) error {
	foodID, err := c.ParamsInt("id")
	var request UpdateFoodRequest
	if err = c.BodyParser(&request); err != nil {
		c.SendStatus(fiber.StatusBadRequest)
	}
	var updatedFood Food
	updatedFood,err = h.service.UpdateFood(foodID, request)
	if err != nil {
		return c.SendStatus(fiber.StatusExpectationFailed)
	}
	return c.Status(fiber.StatusOK).JSON(updatedFood)
}
func (h *Handler) RegisterRoutes(app *fiber.App) {
	app.Get("/foods",h.GetFoods)
	app.Post("/foods",h.AddFood)
	app.Get("/foods/:id",h.GetFoodByID)
	app.Delete("/foods/:id",h.DeleteFood)
	app.Put("/foods/:id",h.UpdateFood)
}